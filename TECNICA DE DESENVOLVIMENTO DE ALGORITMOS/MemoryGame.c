// Bibliotecas:
#include <stdio.h> // Biblioteca para o printf e scanf.
#include <stdlib.h> // Biblioteca para o rand() e system cls.
#include <time.h> // Biblioteca para o srand() e clock.
#include <locale.h> // Biblioteca para a regionalizacao
#include <windows.h> // Biblioteca para o sleep.

//Tabuleiro:
#define N 4 // Largura do Tabuleiro (Linhas).
#define M 4 // Comprimento do Tabuleiro (Colunas).
#define P 8 // Quantidade de Cartas Diferentes (Metade do Total).

struct rank {
	char usuario[100];
	int pontuacao;
}ranking;

// Variaveis Globais:
char GAB [N][M]; // Tabuleiro Gabarito.
char TAB [N][M]; // Tabuleiro Apresentado.
char CAR [P] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'}; // Cartas.
int COL [M]; // Numero de Colunas.
int score = 0; // Pontuacao
int acao = 0; // Acoes  

void menu();
void esconder();
void inicializacao();
void embaralhar ();
void telajogo (int acao);
void atualizartela(int acao);
void jogar (int acao);
void ordem();
void usuario();
void instrucoes();
void creditos();
void rank();
void saida();
void teste(int acao);

int main() {
	// Comando de Regionalizacao:
	setlocale(LC_ALL, "Portuguese");
	// Titulo do Programa:
	SetConsoleTitle("Jogo da Memoria");
	// Ordem de Execucao:
	ordem();
	return 0;
}

void menu(){

	do{
		system("color 97");
		printf("\t****************************************************************\n"); 
		printf("\t****************************************************************\n");
	    printf("\t**                     Jogo da Memória                        **\n");
		printf("\t****************************************************************\n"); 
		printf("\t****************************************************************\n");
		printf("\n\t\t\tSELECIONE UMA OPÇÃO:\n");
		printf("\t\t\t1 - INICIAR\n");
		printf("\t\t\t2 - INSTRUÇÕES\n");
		printf("\t\t\t3 - CRÉDITOS\n");
		printf("\t\t\t4 - RANKING\n");
		printf("\t\t\t5 - SAIR\n");
		
		printf("\t\t\tOpção selecionada: ");
		scanf("%d", &acao);
		system("cls");
			
		if(acao == 2){
		instrucoes();
		}
		else if(acao == 3){
		creditos();
		}
		else if(acao == 4){
		rank();
		}
		else if(acao == 5){
		saida();
		}
		else if(acao <=0 || acao>5){
			system("cls");
			printf("\n\n\n\n\n\n\n\n\n\n");
			printf("\t\t\tVOCE DIGITOU UMA OPÇÃO INVALIDA, TENTE NOVAMENTE\n\n");
			sleep(2);
			system("cls");
		}
	}while(acao>1 && acao !=5);
}
// Funçao p/ Esconder as Cartas:
void esconder() {
	int i, j;
	for (i=0; i<N; i++) 
		for (j=0; j<M; j++)
			TAB[i][j]='*';
}
// Função p/ Inicializar o Tabuleiro:
void inicializacao() {
	int i, j, k=0;
	for (i=0; i<N; i++)
		for (j=0; j<M; j++){
			GAB[i][j]=CAR[k];
			if (k<P-1)
				k++;
				else 
					k=0;
		}
	esconder();
}
// Função p/ Embaralhar Cartas:
void embaralhar () {
	int i, j, x, y;
	char aux;
	srand(time(NULL)); // Gerador de Numeros Aleatorios.
	for (i=0; i<N; i++)
		for (j=0; j<M; j++){
			x=rand()%N; // Gera Valores Aleatorios de 0 ate N.
			y=rand()%M; // Gera Valores Aleatorios de 0 ate M.
			aux=GAB[i][j];
			GAB[i][j]=GAB[x][y];
			GAB[x][y]=aux;
		}	
}
// Função p/ Mostrar Tela de Jogo
void telajogo (int acao) {
	int i, j;
	if (acao==1){
		printf ("\n Para Sair, digite: 5 e 5 \n\n\tPontuacao: %d \n\n", score);
		printf ("\t ");
		for (i=0; i<M; i++){
			COL[i]=i;
			printf (" %d\t", COL[i]);
		}
		printf ("\n");
		for (i=0; i<N; i++) {
			printf ("\t%d", i);
			for (j=0; j<M; j++)
				printf ("[%c]\t", TAB[i][j]);
		printf ("\n\n");
		}
	}	
}
// Funçao p/ Atualizar a Tela
void atualizartela(int acao) {
	Sleep (1000); // Para o Programa por 1 Segundo (1000ms).
	system ("cls");
	telajogo(acao);
}
// Funcao p/ Execução do Jogo
void jogar (int acao) {
	int i, j, x, y, virar=0, checar=1;
	while (score<8 || acao==1){ 
		if(score == 8){
			printf("\n\nParabens, voce ganhou.");
			sleep(2);
			break;
		}
		do {   			
			printf ("\n> Informe as Coordenadas (Linha Coluna) da 1° Carta: ");
			scanf ("%d %d", &i, &j);
			if (i==5 && j==5){
				acao=-1;
				break;
			}
			if (i>=N || j>=M){
				printf ("Coordenada Invalida!");
				checar=0;
				atualizartela(acao);
			}
				else 
					checar=1;	
			if (TAB[i][j]=='*' && checar==1){
				TAB[i][j]=GAB[i][j];
				system ("cls");
				telajogo(acao);
				virar++;
			}
				else if (checar==1){
						printf ("Esta carta já foi escolhida!");
						atualizartela(acao);
				}
		} 
		while (virar < 1);
		if (acao<0)
			break;
		do {
			void teste(int acao);
		} while (virar<2);	
		if (acao<0)
			break;
		if (TAB[i][j]==TAB[x][y]){
			virar=0; 
			score++;
			printf ("\n\t\t\t\t  ACERTOOOOOOU!");
			Sleep (500); // Para o Programa por Meio Segundo (500ms).
		}
			else { 
				TAB[i][j]='*';
				TAB[x][y]='*';
				virar=0;
				printf ("\n\t\t\t\t  ERROOOOOOU!");
				Sleep (300); // Para o Programa por 0.3 Segundos (300ms).
			}
		atualizartela(acao);
	}
	FILE  * arquivo = NULL;
			char * usr, * svit;
			int vit = 0;
			char texto[100];
	arquivo = fopen("ranking.txt", "a+");
	ranking.pontuacao = score;
	fprintf(arquivo, "%s ,%d\n",ranking.usuario,ranking.pontuacao);
	fflush(arquivo);
	fclose(arquivo);
	system("cls");
	acao = 0;
	score = 0;
	ordem();
}
//Funcao p/ ordenar o programa
void ordem(){
	usuario();
	menu();	
    inicializacao();
	embaralhar();
	telajogo(acao);
	jogar(acao);	
}
//Funcao p/ salvar o nome dos jogadores
void usuario(){
	system("color 97");
	printf("Digite o nome do Jogador 1: ");
	gets(ranking.usuario);
	fflush(stdin);
	system("cls");
}
//Funcao p/ instrucoes
void instrucoes(){	
	system("cls");
	printf("\n\n\n\n\n\n\n\n\n");
	printf("\t\t\t****************************************************************************\n");
	printf("\t\t\t**                          O QUE É O JOGO?                               **\n");
	printf("\t\t\t**          O jogo da memoria e constituido por uma matriz 4x4            **\n");
	printf("\t\t\t**                    Dividido em 8 letras de A-H                         **\n");
	printf("\t\t\t****************************************************************************\n");
	printf("\t\t\t**                      O OBEJTIVO DO JOGO?                               **\n");
	printf("\t\t\t**      Decorar onde estão as letras e achar seus pares                   **\n");
	printf("\t\t\t**                Quantos mais acertos, mais pontos!                      **\n");
	printf("\t\t\t****************************************************************************\n");
	sleep(2);
	printf("\n\n");
	system("pause");
	system("cls");
}
//Funcao p/ mostrar os creditos
void creditos() {
	system("cls");
	printf("\n\n\n\n\n\n\n\n\n");
	printf("\t\t\t****************************************************************************\n");
	printf("\t\t\t**	Desenvolvido por: Victor, Allison e Daniel.		  **\n");
	printf("\t\t\t****************************************************************************\n");
	printf("\t\t\t**	Jogo da memoria com fins totalmente educativos.      		  **\n");
	printf("\t\t\t****************************************************************************\n");
	sleep(2);
	printf("\n\n");
	system("pause");
	system("cls");	
}
//Funcao p/ o sistema de ranking
void rank() {
	system("cls");
	FILE  * arquivo = NULL;
	char * usr, * svit;
	int vit = 0;
	char texto[100];
	arquivo = fopen("ranking.txt","a+");
	fflush(arquivo);
	printf("\n\n\n\n\n\n\n\n\n");
	printf("\t\t\t****************************************************************************\n");
	printf("\t\t\t**                Ranking                                                 **\n");
	printf("\t\t\t****************************************************************************\n");
	while(fgets(texto,100,arquivo) != NULL) {
		usr = strtok(texto, ",");
		svit = strtok(NULL, ",");
		vit = atoi(svit);
		printf("Usuário: %s, Pontos: %d\n",usr, vit);
	}
	sleep(2);
	printf("\n\n");
	system("pause");
	system("cls");
}
//Funcao p/ saida do programa
void saida() {
	system ("cls");
	if (score==0)
	printf ("\n\n\tJogo encerrado.\n\n");
	else {
		if (score==8)
			printf ("\n\tPARABÉNS, VOCÊ GANHOU!"); 
	printf ("\n\n\t Fim de Jogo.\n\t Pontuacao Final: %d\n\n", score);
	}	
	exit(0);
}

void teste (int acao) {
	int i, j, x, y, virar=0, checar=1;
	
	printf ("\n> Informe as Coordenadas (Linha Coluna) da 2° Carta: ");
	scanf ("%d %d", &x, &y);	
	if ((x == 5 && y == 5) || score == 8){
		acao=-1;

	}
	if (x>=N || y>=M){
		printf ("Coordenada Inválida!");
		checar=0;
		atualizartela(acao);
	}
	else 
		checar=1;
	if (TAB[x][y]=='*' && checar==1){
		TAB[x][y]=GAB[x][y];
		system ("cls");
		telajogo(acao);
		virar++;
	}
	else 
		if (checar==1){
			printf ("Esta carta já foi escolhida!");
			atualizartela(acao);	
		}
	
}